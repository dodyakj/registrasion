{
    'name': 'Registrasion',
    'version': '14.0',
    'description': 'Registrasion',
    'summary': 'Registrasion',
    'author': 'dody',
    'website': 'dodyakj.com',
    'license': 'LGPL-3',
    'category': 'file',
    'depends': [
        'base',
    ],
    'data': [
        'security/ir.model.access.csv',
        'views/views.xml',
    ],
    'auto_install': False,
    'application': False,
}